#!/usr/bin/env python
# -*- coding: utf8 -*-

# Module Imports
import sys
import logging
from logging.handlers import RotatingFileHandler
import argparse
import requests
from requests.auth import HTTPBasicAuth
import pymysql
import configparser

formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger('pradettes_synchro')
handler = RotatingFileHandler('pradettes_synchro.log', maxBytes=200000, backupCount=10)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.DEBUG)

class GaletteConnector(object):
    """docstring for GaletteConnector
    """
    def __init__(self, user, pwd, host, db):
        super(GaletteConnector, self).__init__()

        # Create a connection object
        conn  = pymysql.connect(host=host, user=user, password=pwd, database=db, charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

        # Get Cursor
        self.cur_galette = conn.cursor()

    def get_users(self):
        logger.info(f"Retrieving galette users...")
        users = {}
        self.usernames=[]

        self.cur_galette.execute("SELECT galette_adherents.login_adh,galette_adherents.mdp_adh,galette_adherents.email_adh,galette_adherents.prenom_adh,galette_adherents.nom_adh,galette_adherents.activite_adh,galette_groups.group_name FROM galette_adherents inner join galette_groups_members on galette_adherents.id_adh=galette_groups_members.id_adh inner join galette_groups on galette_groups_members.id_group=galette_groups.id_group")
        result = self.cur_galette.fetchall()

        for row in result:
            if not row['login_adh'] in users:
                user = {'username' : row['login_adh'],
                        'display' : f"{row['prenom_adh']} {row['nom_adh']}",
                        'email' : row['email_adh'],
                        'password' : row['mdp_adh'],
                        'groups': [row['group_name'].split(' ')[0]],
                        'disabled': row['activite_adh']}
                users[row['login_adh']] = user
                self.usernames.append(row['login_adh'])
            else:
                users[row['login_adh']]['groups'].append(row['group_name'].split(' ')[0])
        logger.info(f"Retrieving galette users... DONE")

        return users.values()

    def user_exists(self, user):
        return user in self.usernames

    def get_groups(self):
        logger.info(f"Retrieving galette groups...")
        self.cur_galette.execute("SELECT group_name FROM galette_groups")
        result = self.cur_galette.fetchall()
        logger.info(f"Retrieving galette groups... DONE")
        return [res['group_name'].split(' ')[0] for res in result]

    def close(self):
        self.cur_galette.close()


class NextCloudRestConnector(object):
    def __init__(self,  url, user, pwd, asso):
        super(NextCloudRestConnector, self).__init__()
        self.headers={'OCS-APIRequest': 'true'}
        self.url = url
        self.user = user
        self.pwd = pwd
        self.asso = asso

        response = self.get(f"{self.url}/ocs/v1.php/cloud/groups/{self.asso}?format=json")
        if response.status_code == 200:
            self.users = response.json()['ocs']['data']['users']
        else:
            logger.error(f"Retrieving users on url {self.url} FAILED")

        response = self.get(f"{self.url}/ocs/v1.php/cloud/groups?format=json")
        if response.status_code == 200:
            self.groups = response.json()['ocs']['data']['groups']
        else:
            logger.error(f"Retrieving groups on url {self.url} FAILED")

    def get(self, request):
        logger.info(f"Getting {request} ")
        return requests.get(request, auth=HTTPBasicAuth(self.user, self.pwd), headers=self.headers)

    def post(self, request, data):
        logger.info(f"Posting {request} with {data}")
        return requests.post(request, auth=HTTPBasicAuth(self.user, self.pwd), headers=self.headers, data=data)

    def put(self, request, data):
        logger.info(f"Putting {request} with {data}")
        return requests.put(request, auth=HTTPBasicAuth(self.user, self.pwd), headers=self.headers, data=data)

    def delete(self, request, data=None):
        logger.info(f"Deleting {request} with {data}")
        return requests.delete(request, auth=HTTPBasicAuth(self.user, self.pwd), headers=self.headers, data=data)

    def user_exists(self, user):
        logger.info(f"Checking user exists: {user['username']}")
        return user['username'] in self.users

    def get_users(self):
        response = self.get(f"{self.url}/ocs/v1.php/cloud/groups/{self.asso}?format=json")
        self.users = response.json()['ocs']['data']['users']
        return self.users

    def get_groups(self):
        response = self.get(f"{self.url}/ocs/v1.php/cloud/groups?format=json")
        self.groups = response.json()['ocs']['data']['groups']
        return self.groups

    def group_exists(self, group):
        logger.info(f"Checking group exists: {self.asso}.{group}")
        return f"{self.asso}.{group}" in self.groups

    def add_user(self, user):
        if user['password'] == '':
            logger.warning(f"Adding user {user['username']} with generated password")
            user['password'] = f"next-{user['username']}-pwd"

        logger.info(f"Adding user {user['username']}")
        response = self.post(f"{self.url}/ocs/v1.php/cloud/users", {'userid':user['username'], 'password':user['password'], 'groups[]':{self.asso}})
        logger.info(f"Adding user {user['username']}, result {response.status_code}")
        if response.status_code == 200:
            self.update_user(user)
        else:
            logger.error(f"Adding user {user['username']} FAILED")

    def update_user(self, user):
        logger.info(f"Updating user {user['username']}")
        data = {'key': 'email','value': user['email']}
        response = self.put(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}", data=data)
        logger.info(f"Updating user {user['username']} email, result {response.status_code}")
        
        data = {'key': 'displayname','value': user['display']}
        response = self.put(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}", data=data)
        logger.info(f"Updating user {user['username']} display name, result {response.status_code}")

        if user['password'] != "":
            data = {'key': 'password','value': user['password']}
            response = self.put(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}", data=data)
            logger.info(f"Updating user {user['username']} password, result {response.status_code}")

        if user['disabled'] == 0:
            response = self.put(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}/disable", data=None)
            logger.info(f"Disabling user {user['username']}, result {response.status_code}")
        elif user['disabled'] == 1:
            response = self.put(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}/enable", data=None)
            logger.info(f"Enabling user {user['username']}, result {response.status_code}")
        else:
            logger.error(f"Disabling/Enabling user {user['username']}, status {user['disabled']} unknown")


        response = self.get(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}/groups?format=json")

        next_groups = response.json()['ocs']['data']
        logger.debug(f"nextcloud groups: {next_groups}")
        if 'groups' in next_groups:
            for group in next_groups['groups']:
                # Strip asso name from group name to check equality
                if f"{self.asso}." in group:
                    group = group.split('.')[1]
                if group != self.asso and group not in user['groups']:
                    data = {'groupid': group}
                    response = self.delete(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}/groups", data)
                    logger.info(f"Removing user {user['username']} from group {group}, result {response.status_code}")

        data = {'groupid': self.asso}
        response = self.post(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}/groups", data=data)
        logger.info(f"Adding user {user['username']} to group {self.asso}, result {response.status_code}")

        for group in user['groups']:
            data = {'groupid': f"{self.asso}.{group}"}
            response = self.post(f"{self.url}/ocs/v1.php/cloud/users/{user['username']}/groups", data=data)
            logger.info(f"Adding user {user['username']} to group {self.asso}.{group}, result {response.status_code}")

    def remove_user(self, user):
        if user != "admin":
            logger.info(f"Removing user {user}")
            response = self.delete(f"{self.url}/ocs/v1.php/cloud/users/{user}")
            logger.info(f"Removing user {user}, result {response.status_code}")

    def add_group(self, group):
        logger.info(f"Adding group {self.asso}.{group}")
        response = self.post(f"{self.url}/ocs/v1.php/cloud/groups", {'groupid':f"{self.asso}.{group}"})
        logger.info(f"Adding group {self.asso}.{group}, result {response.status_code}")

    def remove_group(self, group):
        if group != "admin":
            logger.info(f"Removing group {self.asso}.{group}")
            response = self.delete(f"{self.url}/ocs/v1.php/cloud/groups/{self.asso}.{group}")
            logger.info(f"Removing group {self.asso}.{group}, result {response.status_code}")

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--conf", default="galette.conf",
                        help="defines the connection info config file")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")

    args = parser.parse_args()

    logger_stdout = logging.getLogger('pradettes_synchro_stdout')
    logger_stdout.addHandler(logging.StreamHandler(sys.stdout))
    if args.verbose:
        logger_stdout.setLevel(logging.DEBUG)
    else:
        logger_stdout.setLevel(logging.ERROR)

    config = configparser.ConfigParser()
    config.read(args.conf)

    if config.has_section('galette'):
        if config.has_option('galette','user') and config.has_option('galette','password') and config.has_option('galette','host') and config.has_option('galette','database'):
            galette = GaletteConnector(config['galette']['user'], config['galette']['password'], config['galette']['host'], config['galette']['database'])
        else:
            logger.error(f"galette section does not contain mandatory info user/password/host/database {args.conf}")
            sys.exit(-1)
    else:
        logger.error(f"galette section missing in config file {args.conf}")
        sys.exit(-1)

    if config.has_section('nextcloud'):
        if config.has_option('nextcloud','url') and config.has_option('nextcloud','user') and config.has_option('nextcloud','asso'):
            nextcloud = NextCloudRestConnector(config['nextcloud']['url'], config['nextcloud']['user'], config['nextcloud']['password'], config['nextcloud']['asso'])
        else:
            logger.error(f"nextcloud section does not contain mandatory info user/password/url {args.conf}") 
            sys.exit(-1)          
    else:
        logger.error(f"nextcloud section missing in config file {args.conf}")
        sys.exit(-1)

    galette_groups = galette.get_groups()
    for group in galette_groups:
        if not nextcloud.group_exists(group):
            logger.error(f"Galette group {config['nextcloud']['asso']}.{group} does not exist in nextcloud")
            
    # for group in nextcloud.get_groups():
    #   if group not in galette_groups:
    #       nextcloud.remove_group(group)

    galette_users = galette.get_users()
    for user in galette_users:
        if not nextcloud.user_exists(user):
            nextcloud.add_user(user)
        else:
            nextcloud.update_user(user)

    # for user in nextcloud.get_users():
    #   if not galette.user_exists(user):
    #       nextcloud.remove_user(user)

    galette.close()
