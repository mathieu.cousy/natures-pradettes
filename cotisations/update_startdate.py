import sys
import logging
from logging.handlers import RotatingFileHandler
import argparse
import requests
import pymysql
import configparser
from datetime import date

if __name__ == '__main__':

    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger('update_cotisation')
    logger.addHandler(logging.StreamHandler(sys.stdout))


    parser = argparse.ArgumentParser()
    parser.add_argument("--conf", default="galette.conf",
                        help="defines the connection info config file")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.ERROR)

    config = configparser.ConfigParser()
    config.read(args.conf)

    if config.has_section('galette'):
        if not (config.has_option('galette','user') and config.has_option('galette','password') and config.has_option('galette','host') and config.has_option('galette','database')):
            logger.error(f"galette section does not contain mandatory info user/password/host/database {args.conf}")
            sys.exit(-1)
    else:
        logger.error(f"galette section missing in config file {args.conf}")
        sys.exit(-1)


    conn  = pymysql.connect(host=config['galette']['host'], user=config['galette']['user'], password=config['galette']['password'], database=config['galette']['database'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)  
    # Get Cursor
    cur_galette = conn.cursor()

    today = date.today().strftime("%Y-%m-%d")


#    cur_galette.execute(f"UPDATE galette_cotisations SET type_paiement_cotis='7' WHERE type_paiement_cotis='2'")
    result = cur_galette.fetchall()
    logger.error(result)
    # if len(result)>1:
    #     logger.error(f"Mutiple users found for email {email}")
    # elif len(result) == 0:
    #     logger.error(f"No user found for email {email}")
    # else:
    #     id = result[0]['id_adh']
    #     date = payment['date']
    #     cotis = payment['amount']

    #     date = date.split('T')[0]
    #     yy,mm,dd = date.split('-')
    #     logger.info(f"{prenom} {nom} {cotis} le {yy}-{mm}-{dd} id_adh {id}")
    #     date_deb = f"{yy}-{mm}-{dd}"
    #     date_fin = f"{int(yy)+1}-{mm}-{dd}"
    #     # req = f'INSERT INTO galette_cotisations (id_adh, id_type_cotis, montant_cotis, type_paiement_cotis, date_enreg, date_debut_cotis, date_fin_cotis) VALUES ({id},1,{cotis},2,"{date_deb}","{date_deb}","{date_fin}")'
    #     logger.info(f"requête galette {req}")
    #     # cur_galette.execute(req)


    conn.commit()
    conn.close()
