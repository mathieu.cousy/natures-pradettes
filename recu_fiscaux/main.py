#!/usr/bin/env python
# -*- coding: utf-8 -*-
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename
from string import Template

from fpdf import FPDF
import csv
import num2words
import smtplib


def print_pdf(numero, donateur_name=None, donateur_adress=None, amount=None, adhesion=None, date_don=None,
              mode_don=None,
              date_certif=None):
    pdf = FPDF()
    pdf.add_page()

    pdf.image("natures2.png", w=30, h=30)

    pdf.set_x(70)
    pdf.set_font('Arial', 'B', 12)
    pdf.set_text_color(r=46, g=105, b=84)
    pdf.write(10, u"Reçu Fiscal pour don\n")
    pdf.set_font('Arial', '', 12)
    pdf.set_text_color(r=0, g=0, b=0)
    pdf.set_x(40)
    pdf.write(10, u"(Articles 200 et 238 bis du Code général des impôts)\n\n")

    pdf.set_font('Arial', 'B', 12)
    pdf.write(5, u"Association N.A.T.U.R.E.S. Pradettes\n")
    pdf.set_font('Arial', '', 10)
    pdf.write(5, u"C/o Collectif des associations 12 rue Julien Forgues\n"
                 u"31100 Toulouse\n"
                 u"N RNA : W313033560\n"
                 u"SIRET : 89032792700012\n\n"
                 u"Objet : financement d'une association d'intérêt général concourant à la défense de l'environnement naturel.")

    pdf.set_x(60)
    pdf.set_font('Arial', 'BU', 12)
    pdf.write(20, f"Numéro d'ordre de reçu : {numero}\n")

    pdf.set_font('Arial', 'B', 10)
    pdf.write(10, f"Donateur : \n")
    pdf.write(5, f"{donateur_name} \n")
    pdf.set_font('Arial', '', 10)
    pdf.write(5, f"{donateur_adress} \n")

    pdf.set_font('Arial', 'B', 10)
    pdf.write(10, f"Bénéficiaire :\n")
    pdf.write(5, "L'association N.A.T.U.R.E.S. Pradettes :\n")

    pdf.set_font('Arial', '', 10)
    pdf.set_x(20)
    literal = num2words.num2words(amount, lang='fr')
    literal_adh = num2words.num2words(adhesion, lang='fr')
    pdf.write(5,
              f"- reconnaît avoir reçu, au titre des versements ouvrant droit à une réduction d'impôts, la somme de : {literal} ({amount}) Euros, (dont {literal_adh} ({adhesion}) Euros d'adhésion)\n")
    pdf.set_x(20)
    pdf.write(5,
              "- certifie sur l'honneur que les dons qu'elle reçoit ouvrent droit à la réduction d'impôt prévue aux "
              "articles 200 \n")
    # pdf.set_x(20)
    pdf.write(5, "et 238 bis du Code Général des Impôts.\n\n")

    pdf.write(5, f"Date du don : {date_don}\n")
    pdf.write(5, f"Forme du don : Déclaration de don manuel \n")
    pdf.write(5, f"Nature du don : Don numéraire\n")
    pdf.write(5, f"Mode de versement : {mode_don}\n\n\n")

    pdf.set_x(150)
    pdf.write(5, f"Toulouse, le {date_certif}\n")
    pdf.set_x(150)
    pdf.image("signature.jpg", w=40, h=40)
    pdf.write(5, f"Philippe Lebailly Co-président\n\n")

    pdf.set_font('Arial', 'I', 10)
    pdf.write(5, f"Particulier : vous pouvez déduire 66% de votre don dans la limite de 20% de votre revenu imposable.")

    pdf.output(f'output/{donateur_name}.pdf', 'F')


if __name__ == '__main__':

    from_address = "contact@natures-pradettes.org"
    PASSWORD = "@Natures31100"
    server = smtplib.SMTP(host='mail.gandi.net', port=587)
    server.starttls()
    server.login(from_address, PASSWORD)
    server.set_debuglevel(1)

    with open('adh.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        index = 1
        for row in spamreader:
            if row[1] != "Prénom" and row[0] == 'COUSY':
                nom = row[0]
                prenom = row[1]
                email = row[2]
                adresse = row[5]
                cp = row[6]
                adresse = f"{adresse}\n{cp}"
                adhesion = float(row[21].replace(',', '.'))
                total = float(row[20].replace(',', '.'))
                don = 0
                if total != "":
                    don = total - adhesion
                if adhesion > 5:
                    don = don + adhesion - 5
                adhesion = 5

                type_paiement = ''
                if row[15] != "":
                    type_paiement = 'chèque'
                elif row[16] != "":
                    type_paiement = 'Hello Asso'
                elif row[17] != "":
                    type_paiement = 'espèce'

                print_pdf(f"2022-{index}", f"{nom} {prenom}", adresse, total, adhesion, "20/03/2021", type_paiement,
                          "30/01/2022")

                msg = MIMEMultipart()       # create a message

                # setup the parameters of the message
                msg['From'] = from_address
                msg['To'] = email
                msg['Subject']="Reçu fiscal pour votre don à NATURES Pradettes"

                # add in the message body
                with open("message.txt", 'r', encoding='utf-8') as template_file:
                    template_file_content = template_file.read()
                    message_template = Template(template_file_content)
                    message = message_template.substitute(nom=nom, prenom=prenom)
                    msg.attach(MIMEText(message, 'plain'))

                filename = f"output/{nom} {prenom}.pdf"
                with open(filename, "rb") as file:
                    part = MIMEApplication(
                        file.read(),
                        Name=basename(filename)
                    )
                    # After the file is closed
                    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename)
                    msg.attach(part)

                # send the message via the server set up earlier.
                server.sendmail(from_address, email, msg.as_string())
                # server.send_message(msg)

                del msg
                index += 1

    server.quit()
