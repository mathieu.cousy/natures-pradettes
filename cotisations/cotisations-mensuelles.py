import sys
import logging
from logging.handlers import RotatingFileHandler
import argparse
import requests
import pymysql
import configparser
from datetime import date

if __name__ == '__main__':

    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')
    logger = logging.getLogger('pradettes_synchro')
    logger.addHandler(logging.StreamHandler(sys.stdout))


    parser = argparse.ArgumentParser()
    parser.add_argument("--conf", default="galette.conf",
                        help="defines the connection info config file")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.ERROR)

    config = configparser.ConfigParser()
    config.read(args.conf)

    if config.has_section('galette'):
        if not (config.has_option('galette','user') and config.has_option('galette','password') and config.has_option('galette','host') and config.has_option('galette','database')):
            logger.error(f"galette section does not contain mandatory info user/password/host/database {args.conf}")
            sys.exit(-1)
    else:
        logger.error(f"galette section missing in config file {args.conf}")
        sys.exit(-1)

    if config.has_section('helloasso'):
        if not (config.has_option('helloasso','last_import_date') and config.has_option('helloasso','asso') and config.has_option('helloasso','authorization')):
            logger.error(f"helloasso section does not contain mandatory info date / asso / authorization {args.conf}")
            sys.exit(-1)
    else:
        logger.error(f"helloasso section missing in config file {args.conf}")
        sys.exit(-1)

    conn  = pymysql.connect(host=config['galette']['host'], user=config['galette']['user'], password=config['galette']['password'], database=config['galette']['database'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)  
    # Get Cursor
    cur_galette = conn.cursor()

    today = date.today().strftime("%Y-%m-%d")

    request = f"https://api.helloasso.com/v5/organizations/{config['helloasso']['asso']}/payments?from={config['helloasso']['last_import_date']}&to={today}&pageIndex=1&pageSize=200"
    logger.info(f"request: {request}")
    headers = {'accept': 'application/json','authorization': config['helloasso']['authorization']}
    logger.info(f"headers: {headers}")
    response = requests.get(request, headers=headers)
    logger.info(f"answer: {response.json()}")


    if response.status_code == 200:
        payments = response.json()['data']
        for payment in payments:
            email = payment['payer']['email']

            cur_galette.execute(f"SELECT id_adh FROM galette_adherents WHERE email_adh='{email}'")
            result = cur_galette.fetchall()
            if len(result)>1:
                logger.error(f"Mutiple users found for email {email}")
            elif len(result) == 0:
                logger.error(f"No user found for email {email}")
            else:
                id = result[0]['id_adh']
                date = payment['date']
                cotis = payment['amount']

                date = date.split('T')[0]
                yy,mm,dd = date.split('-')
                logger.info(f"{prenom} {nom} {cotis} le {yy}-{mm}-{dd} id_adh {id}")
                date_deb = f"{yy}-{mm}-{dd}"
                date_fin = f"{int(yy)+1}-{mm}-{dd}"
                # req = f'INSERT INTO galette_cotisations (id_adh, id_type_cotis, montant_cotis, type_paiement_cotis, date_enreg, date_debut_cotis, date_fin_cotis) VALUES ({id},1,{cotis},7,"{date_deb}","{date_deb}","{date_fin}")'
                logger.info(f"requête galette {req}")
                # cur_galette.execute(req)
        
        conn.commit()

        # On met la date du jour comme dernier import pour ne pas ré importer
        config['helloasso']['last_import_date'] = today
        with open(args.conf, 'w') as configfile:
            config.write(configfile)
    else:
        logger.error(f"Retrieving payments via {request} FAILED {response.status_code}")

    conn.close()

