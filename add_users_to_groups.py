#!/usr/bin/env python
# -*- coding: utf8 -*-

import pymysql
import re

host = "localhost"
user = "galette"
pwd = "XFlr5Tintin"
db="galette"

conn  = pymysql.connect(host=host, user=user, password=pwd, database=db, charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
cur_galette = conn.cursor()


cur_galette.execute("SELECT id_adh,info_adh FROM galette_adherents")
result = cur_galette.fetchall()
for row in result:
	gs = re.split(',|-', row['info_adh'])
	row_groups = [f'com-{int(g):02}' for g in gs if not g == "x"]
	for group in row_groups:
		cur_galette.execute(f"SELECT id_group from galette_groups where group_name='{group}'")
		id_group = cur_galette.fetchall()
		cur_galette.execute(f"INSERT INTO galette_groups_members(id_group,id_adh) VALUES ('{id_group[0]['id_group']}','{row['id_adh']}')")

conn.commit()
cur_galette.close()