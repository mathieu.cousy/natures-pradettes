import sys
import logging
from logging.handlers import RotatingFileHandler
import argparse
import requests
from requests.auth import HTTPBasicAuth
import pymysql
import configparser
import csv

if __name__ == '__main__':

	formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
									  datefmt='%Y-%m-%d %H:%M:%S')
	logger = logging.getLogger('pradettes_synchro')
	logger.addHandler(logging.StreamHandler(sys.stdout))


	parser = argparse.ArgumentParser()
	parser.add_argument("--conf", default="galette.conf",
						help="defines the connection info config file")
	parser.add_argument("-v", "--verbose", help="increase output verbosity",
					action="store_true")

	args = parser.parse_args()

	if args.verbose:
		logger.setLevel(logging.DEBUG)
	else:
		logger.setLevel(logging.ERROR)

	config = configparser.ConfigParser()
	config.read(args.conf)

	if config.has_section('galette'):
		if config.has_option('galette','user') and config.has_option('galette','password') and config.has_option('galette','host') and config.has_option('galette','database'):
			pass
		else:
			logger.error(f"galette section does not contain mandatory info user/password/host/database {args.conf}")
			sys.exit(-1)
	else:
		logger.error(f"galette section missing in config file {args.conf}")
		sys.exit(-1)

	conn  = pymysql.connect(host=config['galette']['host'], user=config['galette']['user'], password=config['galette']['password'], database=config['galette']['database'], charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
  
	# Get Cursor
	cur_galette = conn.cursor()

	with open('export-natures-01_09_2020-18_06_2021.csv', newline='') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
		row_nb = 0
		belongs = {}
		for row in spamreader:
			if row_nb < 1:
				row_nb += 1
			elif len(row) > 0:
				nom = row[12]
				prenom = row[13]
				cotis = row[5]
				date = row[4]
				email = row[19]

				cur_galette.execute(f"SELECT id_adh FROM galette_adherents WHERE email_adh='{email}'")
				result = cur_galette.fetchall()
				if len(result)>1:
					logger.error(f"Mutiple users found for email {email}")
				elif len(result) == 0:
					logger.error(f"No user found for email {email}")
				else:
					id = result[0]['id_adh']
					date = date.split(' ')[0]
					dd,mm,yy = date.split('/')
					#logger.info(f"{prenom} {nom} {cotis} le {yy}-{mm}-{dd} id_adh {id}")
					date_deb = f"{yy}-01-01"
					date_fin = f"{int(yy)+1}-{mm}-{dd}"
					req = f'INSERT INTO galette_cotisations (id_adh, id_type_cotis, montant_cotis, type_paiement_cotis, date_enreg, date_debut_cotis, date_fin_cotis) VALUES ({id},1,{cotis},7,"{date_deb}","{date_deb}","{date_fin}")'
					#logger.info(f"{req}")
					cur_galette.execute(req)
	conn.commit()
	conn.close()
