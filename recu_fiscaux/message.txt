Bonjour ${prenom} ${nom},

Vous trouverez ci-joint un reçu fiscal pour votre don à l'association NATURES Pradettes. Nous remercions pour votre participation.
En cas d'erreur dans le document, merci de nous contacter par mail à l'adresse : contact@natures-pradettes.org

Naturellement,
L'association NATURES Pradettes.